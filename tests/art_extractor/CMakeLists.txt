add_executable(art_extractor_test
    art_extractor_test.cpp
)
target_link_libraries(art_extractor_test
    Qt5::Test
    thumbnailer-static
    testutils
    gtest)
add_test(art_extractor art_extractor_test)
